package com.example.moviecatalogservice.models;

public class CatalogItem {
    private String name;
    private String desc;
    private int rathing;

    public CatalogItem(String name, String desc, int rathing) {
        this.name = name;
        this.desc = desc;
        this.rathing = rathing;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getRathing() {
        return rathing;
    }

    public void setRathing(int rathing) {
        this.rathing = rathing;
    }
}
